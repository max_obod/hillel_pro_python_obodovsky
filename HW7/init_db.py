import sqlite3
from variables import URL

connection = sqlite3.connect(URL)

def editor(file: str):

    f1 = open(file)
    index_lst =[]
    height_lst = []
    weight_lst = []
    lines = f1.readlines()
    for i in lines:
        res = i.split()
        for j in res:
            if j == res[0]:
                index_lst.append(j)
            elif j == res[1]:
                height_lst.append(j)
            else:
                weight_lst.append(j)
    index_lst.remove(index_lst[0])
    height_lst.remove(height_lst[0])
    weight_lst.remove(weight_lst[0])
    return index_lst, height_lst, weight_lst


index_lst, height_lst, weight_lst = editor('hw.csv')


def replacer(lst: list):

    new_lst = []
    for i in lst:
        res = i.replace(',', '')
        new_lst.append(res)
    return new_lst


def metric(lst: list, inch = 2.54, funt = 0.409517):

    new_lst = []
    for i in lst:
        index = i.find('.')
        if index == 3:
            res = float(i) * funt
            new_lst.append(res)
        else:
            res = float(i) * inch
            new_lst.append(res)
    return new_lst


ind_numb = replacer(index_lst)
weight_pound = replacer(weight_lst)
height_inches = replacer(height_lst)
weight_kg = metric(weight_pound)
height_cm = metric(height_inches)

def db_insert(index, weight, height):

    connection = sqlite3.connect(URL)
    cursor = connection.cursor()
    print("Connect SQLite")
    for i in range(len(index)):
        Ind_num = index[i]
        Weight_KG = weight[i]
        Height_CM = height[i]
        cursor.execute("""INSERT INTO Students
                            (Ind_num, Weight_KG, Height_CM)
                            VALUES (?, ?, ?);""", (Ind_num, Weight_KG, Height_CM))
    connection.commit()
    print("Insert is done")

    cursor.close()


insert = db_insert(ind_numb, weight_kg, height_cm)
