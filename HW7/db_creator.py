import sqlite3


def db_creator():
    connection = sqlite3.connect('students.db')
    cursor = connection.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS Students
                    (id INTEGER PRIMARY KEY AUTOINCREMENT, Ind_num INT, Weight_KG FLOAT, Height_CM FLOAT);''')

    connection.commit()
    connection.close()


create = db_creator()
