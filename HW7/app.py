import sqlite3

from flask import Flask, render_template
from werkzeug.exceptions import abort

from variables import URL, DEBUG

app = Flask(__name__)

def get_db_connection():

    connection = sqlite3.connect(URL)
    connection.row_factory = sqlite3.Row
    return connection


def get_student(Ind_num):
    connection = get_db_connection()
    students = connection.execute('SELECT * FROM students WHERE id = ?', (Ind_num)).fetchone()
    connection.close()
    if students is None:
        abort(404)
    else:
        return students


@app.route("/")
def index():
    return render_template('index.html')


@app.route("/students/list")
def students():
    connection = get_db_connection()
    students = connection.execute('SELECT * FROM students;').fetchall()
    connection.close()
    return render_template('student/list.html', students = students)


@app.route("/avr_data")
def get_avr_data():
    connection = get_db_connection()
    cursor = connection.cursor()
    weight = cursor.execute('SELECT AVG(Weight_KG) FROM Students').fetchone()[0]
    height = cursor.execute('SELECT AVG(Height_CM) FROM Students').fetchone()[0]
    average = f'Average weight is {weight} kg and height is {height} cm.'
    connection.close()
    return render_template('student/avarage.html', avarage = average)


@app.route("/requirements")
def get_req():
    file = open('requirements.txt', 'r', encoding='utf-16', newline='\r\n')
    html = file.readlines()
    return render_template('student/requirements.html', html = html)


app.run(debug=DEBUG)
