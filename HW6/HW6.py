class Student:
    grade = "Student"

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname

    def add_grade(self):
        grades_dict = {
            0: "Student",
            1: "Bachelor",
            2: "Master",
            3: "Graduate Student"
        }
        if self.grade not in grades_dict.values():
            self.grade = "Student"
        for u, m in grades_dict.items():
            if self.grade == m:
                if u < 3:
                    u += 1
                    self.grade = grades_dict.get(u)
                    return self.grade
                return f"Your grade {self.grade}, it's maximum grades."


    def display(self):
        return f"Grade of {self.name} {self.surname} is {self.grade}"

S1 = Student("Maxim", "Obodovsky")
print(S1.name)
print(S1.surname)
print(S1.grade)
print(S1.display())
print(S1.add_grade())
print(S1.display())
print(S1.add_grade())
print(S1.add_grade())
print(S1.display())



class Group:
    number = 0
    student_list = []

    def __init__(self, name):
        self.name = name

    def add_student(self, student):
        if isinstance(student, Student):
            self.student_list.append(student)
        self.number = len(self.student_list)
        return self.student_list, self.number

    def display_group(self):
        return f"In our group {self.name}, are {self.number} persons"

G1 = Group("AI-123")
print(G1.display_group())
print(G1.add_student(S1))
print(G1.display_group())
